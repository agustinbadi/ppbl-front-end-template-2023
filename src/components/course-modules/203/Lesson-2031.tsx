import LessonLayout from "@/src/components/lms/Lesson/LessonLayout";
import LessonIntroAndVideo from "@/src/components/lms/Lesson/LessonIntroAndVideo";

import module from "./module203.json";
import Docs2031 from "@/src/components/course-modules/203/Docs2031.mdx";
import MetadataList from "./cardano/MetadataList";

export default function Lesson2031() {
  const slug = "2031";
  const lessonDetails = module.lessons.find((lesson) => lesson.slug === slug);

  return (
    <LessonLayout moduleNumber={203} sltId="203.1" slug={slug}>
      <LessonIntroAndVideo lessonData={lessonDetails} />
      <Docs2031 />
      <MetadataList metadataKey="2023" />
    </LessonLayout>
  );
}
