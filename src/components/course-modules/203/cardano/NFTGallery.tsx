import { PPBL_NFT_METADATA_QUERY } from "@/src/data/queries/metadataQueries";
import { GraphQLNFTMintingTransaction, GraphQLTransactionMetadata } from "@/src/types/cardanoGraphQL";
import { hexToString } from "@/src/utils";
import { useQuery } from "@apollo/client";
import {
  Accordion,
  AccordionButton,
  AccordionIcon,
  AccordionItem,
  AccordionPanel,
  Box,
  Center,
  Flex,
  Grid,
  Heading,
  Spacer,
  Spinner,
  Stack,
  Text,
} from "@chakra-ui/react";
import Image from "next/legacy/image";
import Link from "next/link";
import React from "react";

declare type nftData = {
  tokenName: string;
  assetId: string;
  quantity: number;
  name: string;
  description: string;
  imageURL: string;
};

const nftPolicyID = "2a384dc205a97463577fc98b704b537f680c0eba84126eb7d5857c86";

function resolveNFTList(txData: GraphQLNFTMintingTransaction[]): nftData[] {
  const _result: nftData[] = [];

  // Unreliable: can you see why?
  // How could it be improved?
  txData.forEach((tx: GraphQLNFTMintingTransaction) => {
    const _asset = tx.mint[0].asset.tokenMints[0].asset;
    const _policy = tx.metadata[0].value[nftPolicyID];
    const _tokenHex = _asset.assetName;
    const _tokenName = hexToString(_tokenHex);
    let checkName;
    if (_policy[_tokenName] && _policy[_tokenName].image) {
      checkName = _tokenName;
    } else {
      if (_policy[_tokenHex] && _policy[_tokenHex].image) {
        checkName = _tokenHex;
      } else return;
    }
    const _assetName = checkName;
    // Review at Live Coding on 2023-05-17 or 2023-05-18
    // 1. View GraphQL in GraphQL Playground
    // 2. Dicuss naming conventions
    // 3. Discuss this solution
    // 4. Update course docs
    const _imageSrc = _policy[_assetName].image.join("");
    let _imageURL = _imageSrc;
    if (_imageSrc.substring(0, 4) == "ipfs") {
      _imageURL = "https://ipfs.io/ipfs/" + _imageSrc.substring(7);
    }
    const _name = _policy[_assetName].name;
    const _desc = _policy[_assetName].description;

    const _nft: nftData = {
      tokenName: _tokenName,
      assetId: _asset.assetId.substring(0, 56) + "." + _asset.assetId.substring(56),
      quantity: tx.mint[0].asset.tokenMints[0].quantity,
      name: _name,
      description: _desc,
      imageURL: _imageURL,
    };
    _result.push(_nft);
  });
  return _result;
}

const NFTGallery = () => {
  // use a regular query to pull all assets
  // from metadataqueries and cardanographql queries - make this pretty, make it pop!
  // then add to docs, a message like: view in your wallet and view below to see if you accomplished this
  // Maybe mess with isotope filters?

  const [nftList, setNFTList] = React.useState<nftData[] | undefined>(undefined);

  const { data, loading, error } = useQuery(PPBL_NFT_METADATA_QUERY, {
    variables: {
      tokenPolicyId: nftPolicyID,
    },
  });

  React.useEffect(() => {
    if (data && data.transactions) {
      const _nfts: nftData[] = resolveNFTList(data.transactions);
      setNFTList(_nfts);
    }
  }, [data]);

  if (loading) {
    return (
      <Center p="10">
        <Spinner size="xl" speed="1.0s" />
      </Center>
    );
  }

  if (error) {
    console.error(error);
    return <Heading size="lg">Error loading data...</Heading>;
  }

  return (
    <>
      <Box w="95%" p="2" my="3" border="1px" borderColor="theme.light">
        <Heading textAlign="center">PPBL 2023 NFTs</Heading>
        <Text textAlign="center" mb="5">
          Complete Assignment 203.1 to add yours!
        </Text>
        <Grid templateColumns="repeat(3, 1fr)" gap={5} p="5">
          {nftList?.map((nft: nftData, index: number) => (
            <Center key={index} flexDirection="column" p="2" bg="theme.light" color="theme.dark" borderRadius="md">
              <Text fontSize="xl" py="1">
                {nft.tokenName}
              </Text>
              {nft.imageURL === "https://ipfs.io/ipfs/QmaG8t5tKUUMJQ82jt9FhYnW1F5cSvRyK3wS7ZQFgNvSqy" ? (
                <>
                  <video width="640" height="360" controls>
                    <source src={nft.imageURL} type="video/mp4" />
                    Your browser does not support the video tag.
                  </video>
                </>
              ) : (
                <>
                  <Link href={`https://preprod.cardanoscan.io/token/${nft.assetId}`}>
                    <Image src={nft.imageURL} width={400} height={400} alt="your nft"></Image>
                  </Link>
                </>
              )}
              <Center w="100%" bg="theme.orange" mt="2" p="1">
                <Text fontWeight="bold">{nft.description}</Text>
              </Center>
            </Center>
          ))}
        </Grid>
      </Box>
      <Box w="95%" p="2" my="3" border="1px" borderColor="theme.light">
        <Accordion allowToggle>
          <AccordionItem>
            <AccordionButton>
              <Heading size="md">View Processed NFT Data</Heading>
              <Spacer />
              <AccordionIcon />
            </AccordionButton>
            <AccordionPanel>
              <pre>{JSON.stringify(nftList, null, 2)}</pre>
            </AccordionPanel>
          </AccordionItem>
        </Accordion>
      </Box>
    </>
  );
};

export default NFTGallery;
