import { METADATA_KEY_QUERY } from "@/src/data/queries/metadataQueries";
import { GraphQLFaucetMetadataUTxO } from "@/src/types/cardanoGraphQL";
import { useQuery } from "@apollo/client";
import { Box, Center, Grid, Heading, Spinner } from "@chakra-ui/react";
import FaucetDynamicInstance from "./FaucetDynamicInstance";

type Props = {
  metadataKey: string;
};

//const revocationList = ["06dfbfa4577d8f82c0949d065f59a9cab5a492dab7139b8f6a6fef65e2e70484"]

const FaucetList: React.FC<Props> = ({ metadataKey }) => {
  const { data, loading, error } = useQuery(METADATA_KEY_QUERY, {
    variables: {
      metadatakey: metadataKey,
    },
  });

  // Live Coding 2023-09-13
  // From graphql query:
  // {
  //   "hash": "202656d57d63667d07c0c54e18a367674c59398d462d31b9729fd6069e789c07",
  //   "includedAt": "2023-05-15T13:34:34Z",
  //   "metadata": [
  //     {
  //       "key": "161803398875",
  //       "value": {
  //         "policyId": "37d65871b23a343b6960914ff246a35ca14c8e7fbc08d0f4a9ee2565",
  //         "refUTxOIx": 0,
  // // the following checkFaucetData function filters out Faucet Registration Metadata if token name is hex format
  //         "tokenName": "4e5a2d31",
  //         "aboutToken": [
  //           "NZ-1 is the one",
  //           "The only north zeta token"
  //         ],
  //         "refUTxOHash": "877fbd9f4d40b06df239fdd494bb12f71c132d9ca231a6411fa12bd6931e822f",
  //         "contractAddress": "addr_test1wpykakj38vhwnyccnxy5sqgulsz7dgprx042qpczc3x294cp58q3m",
  //         "withdrawalAmount": 50
  //       }
  //     }
  //   ]
  // },

  if (loading) {
    return (
      <Center p="10">
        <Spinner size="xl" speed="1.0s" />
      </Center>
    );
  }

  if (error) {
    console.error(error);
    return <Heading size="lg">Error loading data...</Heading>;
  }

  return (
    <Box my="3">
      <Grid w="90%" templateColumns="repeat(1, 1fr)" gap="5">
        {data.transactions.map((tx: GraphQLFaucetMetadataUTxO, index: number) => (
          <>
            <Box key={index}>
              <FaucetDynamicInstance faucetMetadata={tx.metadata[0].value} />
            </Box>
          </>
        ))}
      </Grid>
    </Box>
  );
};

export default FaucetList;
