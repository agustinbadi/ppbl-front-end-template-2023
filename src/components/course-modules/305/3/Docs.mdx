import Quiz from "@/src/components/lms/Lesson/Quiz.tsx";
import { CardanoWallet } from "@meshsdk/react";

## Connect Client's Wallet

Suppose you're looking to bring people to your website, where they can
mint an NFT or interact with a smart contract; they won't get very far
unless they can connect their wallet easily. This is where Mesh connect
wallet component comes in, to make it easier for you as a developer to
add these buttons on your website. From the end users' perspective, it
allows them to connect to your websites with just a few clicks. This
section will show how to add a connect wallet button to your website.

This is the [connect wallet](https://meshjs.dev/react/ui-components#connectWallet) button component:

<CardanoWallet />

When you click on this button, you will notice that you can choose to
connect to any wallets you have installed on your device. If it is the
first time connecting to a site, your wallet will prompt and ask you for
permission to allow the website to connect.

Let's look at the code to see how we can use `CardanoWallet`
on our applications:

```js
import { CardanoWallet } from "@meshsdk/react";

const Page = () => {
  return <CardanoWallet />;
};
```

With just a few lines of code, you can add a connect wallet button to
your website. We import the `CardanoWallet` component and use
it your application. Lets look at your starter project (if you have
missed that, follow the previuos lesson to set it up), you can find the code for `CardanoWallet`
component in the [pages/index.tsx](https://github.com/MeshJS/starter-next-ts-template/blob/main/pages/index.tsx) file.

### Mini-project: Build your own Wallet UI component

How does `CardanoWallet` works? We can look at the code
in [CardanoWallet.tsx](https://github.com/MeshJS/mesh/blob/main/packages/react/src/components/CardanoWallet/CardanoWallet.tsx).

We can get a list of all the wallets that are installed on the user's
device with:

```js
const wallets = useWalletList();
```

`useWalletList()` returns a list of wallets installed on user's device.
With the list of wallets available, we can render a list of buttons for the user
to choose from to connect their wallet. This is why when you mouse over the connect
wallet button, you can see a list of wallets available. This is one of the [React
hooks](https://meshjs.dev/react/wallet-hooks) provided by Mesh, and we will learn
more about hooks in the following section.

In [CardanoWallet.tsx](https://github.com/MeshJS/mesh/blob/main/packages/react/src/components/CardanoWallet/CardanoWallet.tsx), we also use the `useWallet()` which provide information on the current
wallet's state, and functions for connecting and disconnecting user wallet.

```js
const { connect, connecting, connected, disconnect, name } = useWallet();
```

To connect to a wallet when user select a wallet, we invoke the function `connect()` provided by `useWallet()`.

That is all you need to know to build your own connect wallet UI component. Go on and try making your own connect wallet UI component!

## Provider and hooks

It is important to note that, we need to set up the [MeshProvider](https://meshjs.dev/react/getting-started) that provides all the context to be consumed by your application.

Open the [pages/\_app.tsx](https://github.com/MeshJS/starter-next-ts-template/blob/main/pages/_app.tsx) file, you will see the following code:

```js
<MeshProvider>
  <Component {...pageProps} />
</MeshProvider>
```

`MeshProvider` is a [React Context](https://reactjs.org/docs/context.html) that allows data to be shared across the app, and `MeshProvider` allows your app to subscribe to context changes. `MeshProvider` provides the current state of the connected wallet. This "state" is call "context", and setting it up is as easy as wrapping your application with `MeshProvider`. With this context, you can use the connected wallet throughout your application using React hooks.

### Wallet Hooks

In a React application, [Hooks](https://react.dev/reference/react) allows you to extract and reuse stateful logic and variables without changing your component hierarchy. This makes it easy to reuse the same Hook among many components.

For example, you can interact with the connected wallet in your pages, such as getting the user's address, or the user's NFTs that they are holding in their wallet. We can use `useWallet()` to get the connected wallet's state.

```js
import { useWallet } from "@meshsdk/react";

const { wallet, connected, name, connecting, connect, disconnect } = useWallet();
```

With `useWallet()`, we can use `wallet`, that is a [Browser Wallet](https://meshjs.dev/apis/browserwallet) instance, which expose all CIP wallets functions from getting assets to signing tranasction. For example, we can get the user's wallet address by using `wallet.getUsedAddresses()`:

```js
async function getAddresses() {
  if (connected && wallet) {
    const usedAddresses = await wallet.getUsedAddresses();
  }
}
```

You can also make use of the available [Wallet hooks](https://meshjs.dev/react/wallet-hooks) provided by Mesh SDK, such as `useLovelace()` to get the user's wallet balance in lovelace, and `useAssets()` to get the user's wallet assets.

<Quiz
  question="Which statement is false?"
  choices={[
    {
      value: "true1",
      label:
        "I can create a connect wallet UI component with `useWalletList()`",
      wrongNote:
        "You can! Using `useWalletList()` to list of wallets installed on user's device.",
    },
    {
      value: "true2",
      label: "I can use `useAssets()` to display user's NFT collection",
      wrongNote: "`useAssets()` returns a list of assets in user's wallet.",
    },
    {
      value: "nottrue",
      label: "Users can connect to multiple wallets using `useWallet()`",
      correctNote:
        "`useWallet()` provides the state of the current connected wallet.",
    },
    {
      value: "true3",
      label:
        "`useNetwork()` allows me to ensure user is connecting their wallet to the correct network",
      wrongNote:
        "`useNetwork()` returns the current network the user is connected to.",
    },
  ]}
  correctAnswer="nottrue"
/>

## Get wallet assets

Here is an example of a simple user interface where user can connect their wallet, and get their wallet balance and assets. Here, we are using `useLovelace()` to get the user's wallet balance and invoking the `wallet.getAssets()` function to get user's assets.

```js
import { useState } from "react";
import type { NextPage } from "next";
import { CardanoWallet, useWallet, useLovelace } from "@meshsdk/react";

const Page: NextPage = () => {
  const { connected, wallet } = useWallet();
  const lovelace = useLovelace();
  const [assets, setAssets] = useState<null | any>(null);
  const [loading, setLoading] = useState<boolean>(false);

  async function getAssets() {
    if (wallet) {
      setLoading(true);
      const _assets = await wallet.getAssets();
      setAssets(_assets);
      setLoading(false);
    }
  }

  return (
    <div>
      <h1>Wallet Integration</h1>
      <CardanoWallet />

      {connected && (
        <>
          <p>Amount lovelace in wallet: {lovelace}</p>
          <h2>Get Wallet Assets</h2>
          {assets ? (
            <pre>
              <code>{JSON.stringify(assets, null, 2)}</code>
            </pre>
          ) : (
            <button
              type="button"
              onClick={() => getAssets()}
              disabled={loading}
              style={{
                margin: "8px",
                backgroundColor: loading ? "orange" : "grey",
              }}
            >
              Get Wallet Assets
            </button>
          )}
        </>
      )}
    </div>
  );
};

export default Page;
```

Try implement this on your own! 

<Quiz
  question="How to get the UTXOs of the connected wallet?"
  choices={[
    { value: "wrong1", label: "getUtxos();" },
    { value: "wrong2", label: "wallet.getUtxos();" },
    { value: "right", label: "await wallet.getUtxos();" },
  ]}
  correctAnswer="right"
  correctNote={<>Try implementing this function to get wallet UTXOs!</>}
  wrongNote={
    <>
      Check{" "}
      <a href="https://meshjs.dev/apis/browserwallet">Mesh documentation</a>.
    </>
  }
/>

## Mini Project - Asset Gallery

In this lesson, you have learned how to connect your wallet, get the wallet state, and get the wallet assets.

This mini project, we will build a simple wallet asset gallery. This will be a simple React application that will allow you to connect your wallet, and view your wallet assets.

### Project Setup

First, we will need to create a new application. You can use one of the starter templates provided by Mesh SDK, such as the [starter-next-ts-template](https://meshjs.dev/starter-templates) or start a new project from scratch using [this guide](https://meshjs.dev/guides/nextjs).

Next, add components such that, when the visitor visits your application, they should be able to connect their wallet and view their wallet assets.

Then, customize the application to your liking. You can change the application name, description, and logo. You can also change the application theme to your liking.

After that, as a bonus, you can add a search bar where users can search for any ADA handler or address, and view the assets that they are holding.

Finally, push your codes on GitHub and deploy your application on Vercel, and showcase your application to the world!

### Useful Resources

- [Guide: Start a Web3 app on Next.js](https://meshjs.dev/guides/nextjs)
- [Browser Wallet](https://meshjs.dev/apis/browserwallet)
- [Mesh React Provider](https://meshjs.dev/react/getting-started)
- [Mesh React Components](https://meshjs.dev/react/ui-components)
- [Mesh React Hooks](https://meshjs.dev/react/wallet-hooks)
- [Starter Template](https://meshjs.dev/starter-templates)
- [Data Providers](https://meshjs.dev/providers)

import MDXLessonLayout from "@/src/components/lms/Lesson/MDXLessonLayout.tsx";
export default ({ children }) => <MDXLessonLayout>{children}</MDXLessonLayout>;
