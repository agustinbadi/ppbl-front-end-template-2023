import { Heading, Box, Link as CLink, Grid, Center, Text, Flex, Spacer, theme } from "@chakra-ui/react";
import { CiLinkedin, CiTwitter } from "react-icons/ci";

import ppblteam from "@/src/data/team/gimbalabs-ppbl-team.json";
import Image from "next/image";

export default function TeamList() {
  return (
    <Box>
      <Heading py="10" size="2xl">
        Plutus PBL Team
      </Heading>
      <Grid templateColumns={{ base: "repeat(1, 1fr)", lg: "repeat(4, 1fr)" }} gap={4}>
        {ppblteam.team.map((contributor: any, index: number) => (
          <Center my="5" key={index} flexDirection="column">
            <Image
              src={contributor.image}
              width="200"
              height="200"
              alt="pfp"
              style={{ borderRadius: "100%", background: "white" }}
            />
            <Text fontSize="2xl" pt="5">
              {contributor.name}
            </Text>
            <Flex w="30%" direction="row" fontSize="2xl" mt="3">
              <Spacer />
              {contributor.linkedIn.length > 0 && (
                <Text _hover={{ color: "theme.yellow" }}>
                  <a href={contributor.linkedIn}>
                    <CiLinkedin />
                  </a>
                </Text>
              )}
              {contributor.linkedIn.length > 0 && contributor.twitter.length > 0 && <Spacer />}
              {contributor.twitter.length > 0 && (
                <Text _hover={{ color: "theme.yellow" }}>
                  <a href={contributor.twitter}>
                    <CiTwitter />
                  </a>
                </Text>
              )}
              <Spacer />
            </Flex>
          </Center>
        ))}
      </Grid>
    </Box>
  );
}
